## a35xue-user 14 UP1A.231005.007 A356U1UEU3AXH3 release-keys
- Manufacturer: samsung
- Platform: erd8835
- Codename: a35x
- Brand: samsung
- Flavor: a35xue-user
- Release Version: 14
- Kernel Version: 5.15.123
- Id: UP1A.231005.007
- Incremental: A356U1UEU3AXH3
- Tags: release-keys
- CPU Abilist: arm64-v8a,armeabi-v7a,armeabi
- A/B Device: false
- Treble Device: true
- Locale: en-US
- Screen Density: undefined
- Fingerprint: samsung/a35xue/a35x:13/TP1A.220624.014/A356U1UEU3AXH3:user/release-keys
- OTA version: 
- Branch: a35xue-user-14-UP1A.231005.007-A356U1UEU3AXH3-release-keys
- Repo: samsung_a35x_dump
